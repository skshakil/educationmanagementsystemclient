export const environment = {
  production: true,
  basePath    : 'educare',
  apiPath     : 'administration',
  baseUrl     : 'http://' + window.location.hostname + ':',
  authApiUrl  : '9010/auth-api'
};
