import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { assetUrl } from 'src/single-spa/asset-url';
import { UserService } from 'src/app/_coreSecuritySettings/services/user-service.service';
import { CoreUserEntity } from 'src/app/_coreSecuritySettings/models/core-user-entity.model';
import { MessageDialogComponent } from '../message-dialog/message-dialog.component';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css'],
})
export class AddUserComponent implements OnInit {
  modalTitle: string = 'Add User';
  submitTitle: string = 'Save';
  receivedUser: any;

  profileAvatar: any;

  coreUserEntity: CoreUserEntity = new CoreUserEntity();
  response: any;
  userModalRef: BsModalRef<MessageDialogComponent> | undefined;
  message: string = '';
  onClose: Subject<any> = new Subject();

  constructor(
    public adduserModalRef: BsModalRef,
    private userService: UserService,
    private modalService: BsModalService
  ) {}

  ngOnInit(): void {
    if (this.receivedUser) {
      this.coreUserEntity = this.receivedUser;
    }
    console.log('ngOnItit called.');
    this.profileAvatar = assetUrl('images/profile-placeholder.jpg');
  }

  submit() {
    if (this.submitTitle == 'Save') {
      this.saveUser();
    } else {
      this.updateUser();
    }
  }

  updateUser() {
    console.log('update user is called.');
    let userObj = {
      user: this.coreUserEntity,
    };

    console.log('userObj: ', userObj);
    this.userService.updateUser(userObj).subscribe(
      (data) => {
        // window.location.reload();
        console.log('update user data: ', data);
        if (data.success) {
          this.adduserModalRef.hide();
          this.onClose.next(true);
        }
      },
      (error) => console.log('error: ' + error)
    );
  }

  saveUser() {
    let userObj = {
      user: this.coreUserEntity,
    };
    this.response = this.userService.saveUser(userObj).subscribe(
      (data) => {
        if (!data.success) {
          this.message =
            'User name already exists. \n Try with another user name';
          this.showMessageDialog();
        }
        if (data.message == null) {
          this.message = 'User Id already exists. \n Try with another user Id';
          this.showMessageDialog();
        }
        if (data.message == 'Saved Successfully') {
          this.adduserModalRef.hide();
          this.onClose.next(true);
          console.log('Saved Successfully');
        }
        console.log(this.coreUserEntity.activeStatus);
        console.log(data);
      },
      (error) => console.log(error)
    );

    console.log(this.response);
  }

  showMessageDialog() {
    MessageDialogComponent.message = this.message;
    this.userModalRef = this.modalService.show(MessageDialogComponent, {
      class: 'modal-sm',
    });
    return;
  }

  selectedOrg!: number;

  organization = [
    { id: 1, name: 'Dhaka Medical' },
    { id: 2, name: 'Rajshai Medical' },
  ];

  selectedcompany!: number;

  company = [
    { id: 1, name: 'Dhaka Medical' },
    { id: 2, name: 'Rajshai Medical' },
  ];

  //   isChecked: any = 1;
  //   checkValue(event: any){
  //     console.log(event);
  //     this.coreUserEntity.activeStatus = event;
  //  }

  checkActiveStatus(event: any) {
    this.coreUserEntity.activeStatus = event;
  }

  checkAccountExpired(event: any) {
    this.coreUserEntity.accountExpired = event;
    console.log(event);
  }

  checkEnabled(event: any) {
    this.coreUserEntity.enabled = event;
  }
}
