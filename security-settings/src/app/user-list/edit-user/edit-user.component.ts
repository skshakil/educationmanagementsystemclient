import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { assetUrl } from 'src/single-spa/asset-url';
import { UserService } from 'src/app/_coreSecuritySettings/services/user-service.service';
import { CoreUserEntity } from 'src/app/_coreSecuritySettings/models/core-user-entity.model';
import { logging } from 'protractor';
import { MessageDialogComponent } from '../message-dialog/message-dialog.component';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  profileAvatar : any;

  coreUserEntity : CoreUserEntity = new CoreUserEntity();

  public static selectedUser: any;
  private id!: number;
  userModalRef: BsModalRef | undefined;

  constructor(
    public edituserModalRef: BsModalRef,
    private userService : UserService,
    private modalService: BsModalService,
    ) { }

  ngOnInit(): void {
    
    console.log("edit ngONInit is called.");
    console.log("user id: " + EditUserComponent.selectedUser.id);
    // this.id = EditUserComponent.selectedUser.id;
    // console.log("id: "+ this.id);
    // this.userService.findUser(EditUserComponent.selectedUser.id).subscribe(data => {
    //   console.log("data: " + data);
    //   this.coreUserEntity = data;
    // }, error => console.log("error: " + error));


    
    this.coreUserEntity = EditUserComponent.selectedUser;
    console.log("coreUserEntity: " + this.coreUserEntity.id);

    console.log("expired: " + this.coreUserEntity.accountExpired);

    this.profileAvatar = assetUrl('images/profile-placeholder.jpg');
  }

  updateUser() {
    console.log("update user is called.");
    let userObj = {
      "user" : this.coreUserEntity
    }

    console.log(userObj);
    this.userService.updateUser(userObj).subscribe(data => {
      // window.location.reload();

      console.log(data);
    }, error => console.log("error: " + error));
  }

  selectedOrg!: number;

  organization = [
      { id: 1, name: 'Dhaka Medical' },
      { id: 2, name: 'Rajshai Medical' },
  ];


  selectedcompany!: number;

  company = [
      { id: 1, name: 'Dhaka Medical' },
      { id: 2, name: 'Rajshai Medical' },
  ];

  
 

  
    checkActiveStatus(event: any){
      this.coreUserEntity.activeStatus = event;
  }

  checkAccountExpired(event: any){
    this.coreUserEntity.accountExpired = event;
    console.log(event);
    
  }

  checkEnabled(event: any){
    this.coreUserEntity.enabled = event;
  }

}
