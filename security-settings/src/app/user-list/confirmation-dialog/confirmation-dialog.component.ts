import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { CoreUserEntity } from 'src/app/_coreSecuritySettings/models/core-user-entity.model';
import { UserService } from 'src/app/_coreSecuritySettings/services/user-service.service';
import { MessageDialogComponent } from '../message-dialog/message-dialog.component';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.css'],
})
export class ConfirmationDialogComponent implements OnInit {
  static selectedUser: any;
  userModalRef: BsModalRef | undefined;
  onClose: Subject<any> = new Subject();

  constructor(
    public deleteUserModalRef: BsModalRef,
    private modalService: BsModalService,
    private coreUserService: UserService
  ) {}

  ngOnInit(): void {}

  public deleteUser(): void {
    this.onClose.next(true);
    // this.coreUserService
    //   .deleteUserById(ConfirmationDialogComponent.selectedUser.id)
    //   .subscribe((data) => {
    //     console.log('deleted successfully.');
    //     window.location.reload();
    //   });
  }
}
