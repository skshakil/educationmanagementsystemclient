import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthService } from 'auth-library';
import { AddUserComponent } from './add-user/add-user.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { UserService } from '../_coreSecuritySettings/services/user-service.service';
import { MessageDialogComponent } from './message-dialog/message-dialog.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
})
export class UserListComponent implements OnInit {
  @ViewChild('usergridList', { static: true }) usergridTable: any;
  usergridList: any;
  usergridTableObj: any;
  selectedUser: any;
  userModalRef: BsModalRef | undefined;
  deleteUserModalRef: BsModalRef | undefined;
  constructor(
    private authService: AuthService,
    private modalService: BsModalService,
    private coreUserService: UserService
  ) { }

  ngOnInit(): void {
    this.initUserGrid();
  }

  initUserGrid() {
    let that = this;
    this.usergridList = $(this.usergridTable.nativeElement);
    this.usergridTableObj = this.usergridList.DataTable({
      pagingType: 'full_numbers',
      pageLength: 10,
      serverSide: true,
      processing: true,
      ajax: {
        url:
          environment.baseUrl +
          environment.authApiUrl +
          '/api/coreUser/gridList',
        type: 'GET',
        data: function (sendData: any) { },
        // beforeSend: function (xhr: any) {
        //   xhr.setRequestHeader(
        //     'Authorization',
        //     'bearer ' + 'd12aec46-f31f-4b73-ab7a-312fd1c7a8f4'
        //   );
        //   xhr.setRequestHeader('Content-Type', 'application/json');
        // },
        beforeSend: function (xhr: any) {
          xhr.setRequestHeader(
            'Authorization',
            'bearer ' + that.authService.getAccessToken()
          );
          xhr.setRequestHeader('Content-Type', 'application/json');
        },
        dataSrc: function (response: any) {
          response.draw = response.obj.draw;
          console.log('Grid: ', response);
          response.recordsTotal = response.obj.recordsTotal;
          response.recordsFiltered = response.obj.recordsFiltered;
          return response.obj.data;
        },
        error: function (request: any) {
          console.log('request.responseText', request.responseText);
        },
      },
      order: [[0, 'DESC']],
      columns: [
        {
          width: '100px',
          title: 'ID',
          data: 'id',
          name: 'id',
        },
        {
          width: '100px',
          title: 'User ID',
          data: 'userId',
          name: 'userId',
        },
        {
          title: 'Name',
          data: 'userName',
          name: 'userName',
          // render: (data : any, type : any, full : any) => {
          //   return full.salutation + ' ' + full.stFName + ' ' + full.stLName;
          // },
        },
        {
          width: '100px',
          title: 'Gender',
          data: 'gender',
        },
        {
          title: 'Account type',
          data: 'userTypeNo',
          render: (data: number) => {
            if (data == 1) {
              return 'ADMIN USER';
            } else if (data == 2) {
              return 'ORGANIZATION USER';
            }
            return data;
          },
        },
        {
          title: 'Nationality',
          data: 'nationality',
        },
      ],
      columnDefs: [
        //hide column
        { visible: false, targets: [] },
      ],
      responsive: true,
      select: true,
      rowCallback: (row: Node, data: any[] | Object) => {
        $(row).unbind('click');
        $(row).bind('click', () => {
          $(row).removeClass('selected');
          $(row).addClass('selected');
          this.onClickGridData(data);
        });
        return row;
      },
    });
  }
  response: any;
  onClickGridData(data: any): void {
    this.response = this.coreUserService.findUser(data.id).subscribe((user) => {
      console.log('req: ', user.obj);
      this.selectedUser = user.obj;
      ConfirmationDialogComponent.selectedUser = user.obj;
    });

    console.log('response: ', this.response);
  }

  // public deleteUser(): void {
  //   if (!this.selectedUser) {
  //     console.log('Pleae select a user !');
  //     return;
  //   }
  //   this.coreUserService.deleteUserById(this.selectedUser.id).subscribe(data => {
  //     console.log("deleted successfully.");
  //     window.location.reload();
  //   })
  //   console.log('User Found ID:', this.selectedUser.id);
  // }

  editUser() {
    console.log('edit user is called.');
    if (this.selectedUser != null) {
      if (this.selectedUser.id) {
        this.selectedUser.accountExpireDate = this.selectedUser
          .accountExpireDate
          ? new Date(this.selectedUser.accountExpireDate)
          : undefined;
      }
      this.adduserModal(this.selectedUser);
      // EditUserComponent.selectedUser = this.selectedUser;
      // this.userModalRef = this.modalService.show(EditUserComponent, {
      //   class: 'modal-lg',
      // });
      // this.userModalRef.content.onClose.subscribe(
      //   (_res: any) => {
      //    if(_res) {
      //     this.usergridTableObj.draw();
      //    }
      //   }
      // );
    } else {
      MessageDialogComponent.message = 'Please select a user first!';
      this.userModalRef = this.modalService.show(MessageDialogComponent, {
        class: 'modal-sm',
      });
      return;
    }
  }

  adduserModal(user?: any) {
    let initialState = {
      modalTitle: user ? 'Edit User' : 'Add User',
      submitTitle: user ? 'Update' : 'Save',
      receivedUser: user,
    };
    this.userModalRef = this.modalService.show(AddUserComponent, {
      initialState,
      class: 'modal-lg',
    });
    this.userModalRef.content.onClose.subscribe((res: any) => {
      if (res) {
        this.usergridTableObj.draw();
      }
    });
  }

  deleteUserModal() {
    this.deleteUserModalRef = this.modalService.show(
      ConfirmationDialogComponent,
      {
        class: 'modal-sm',
      }
    );
  }

  deleteUser() {
    if (this.selectedUser != null) {
      this.deleteUserModalRef = this.modalService.show(ConfirmationDialogComponent, { class: 'modal-sm', });
      this.deleteUserModalRef.content.onClose.subscribe(
        (res: any) => {
          if (res) {
            this.coreUserService.deleteUserById(this.selectedUser.id).subscribe(
              res => {
                console.log(res);
                console.log('deleted successfully');
                window.location.reload();
              }, err => {
                console.log('deleted failed');
              }
            );
          }
        }
      )
    } else {
      MessageDialogComponent.message = 'Please select a user first!';
      this.userModalRef = this.modalService.show(MessageDialogComponent, {
        class: 'modal-sm',
      });
      return;
    }
  }
}
