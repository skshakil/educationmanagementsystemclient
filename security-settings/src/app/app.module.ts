import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { DataTablesModule } from 'angular-datatables';
import { ModalModule, BsModalRef } from 'ngx-bootstrap/modal';
import { 
  AuthGuard, 
  AuthLibraryModule,
  AuthService,
  TokenInterceptorService
} from 'auth-library';
import {
  CoreLibraryModule
} from 'core-library';
import { UserListComponent } from './user-list/user-list.component';
import { AddUserComponent } from './user-list/add-user/add-user.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { ConfirmationDialogComponent } from './user-list/confirmation-dialog/confirmation-dialog.component';
import { EditUserComponent } from './user-list/edit-user/edit-user.component';
import { MessageDialogComponent } from './user-list/message-dialog/message-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    UserListComponent,
    AddUserComponent,
    ConfirmationDialogComponent,
    EditUserComponent,
    MessageDialogComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    DataTablesModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AuthLibraryModule.forRoot(environment),
    CoreLibraryModule.forRoot(environment),
    NgSelectModule
  ],
  entryComponents: [
    AddUserComponent,
    ConfirmationDialogComponent
  ],
  providers: [AuthGuard,AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  DataTablesModule: any;
  ModalModule: any;
  BsDatepickerModule: any;

 }
