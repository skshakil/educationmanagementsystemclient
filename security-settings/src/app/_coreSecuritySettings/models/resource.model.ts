export class Resource {
  id: number | undefined;
  success: boolean | true | undefined;
  info: boolean | false | undefined;
  warning: boolean | false | undefined;
  message: string | undefined;
  valid: boolean | false | undefined;
  obj: any;
}
