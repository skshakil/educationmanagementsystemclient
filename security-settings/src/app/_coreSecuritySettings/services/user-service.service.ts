import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  // private SAVE_USER_URL = 'http://localhost:9001/educare-auth-api/api/coreUser/save';
  // private UPDATE_USER_URL = 'http://localhost:9001/educare-auth-api/api/coreUser/update';
  // private DELETE_USER_URL = 'http://localhost:9001/educare-auth-api/api/coreUser/delete';
  // private FIND_USER_URL = 'http://localhost:9001/educare-auth-api/api/coreUser/find';

  private SAVE_USER_URL =
    environment.baseUrl + environment.authApiUrl + '/api/coreUser/save';
  private UPDATE_USER_URL =
    environment.baseUrl + environment.authApiUrl + '/api/coreUser/update';
  private DELETE_USER_URL =
    environment.baseUrl + environment.authApiUrl + '/api/coreUser/delete';
  private FIND_USER_URL =
    environment.baseUrl + environment.authApiUrl + '/api/coreUser/find';

  constructor(private httpClient: HttpClient) {}

  saveUser(coreUserEntity: any): Observable<any> {
    return this.httpClient.post<any>(this.SAVE_USER_URL, coreUserEntity);
  }

  findUser(id: any): Observable<any> {
    return this.httpClient.get(this.FIND_USER_URL, {
      params: {
        id: id,
      },
    });
  }

  updateUser(coreUserEntity: any): Observable<any> {
    return this.httpClient.put<any>(this.UPDATE_USER_URL, coreUserEntity);
  }

  deleteUserById(id: number): Observable<any> {
    return this.httpClient.post<any>(this.DELETE_USER_URL, id);
  }
}
