import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-add-campus',
  templateUrl: './add-campus.component.html',
  styleUrls: ['./add-campus.component.css']
})
export class AddCampusComponent implements OnInit {

  constructor(
    public campusModalRef: BsModalRef,
  ) { }

  ngOnInit(): void {
  }
  institute = [
    { id: 1, name: 'Institute 1' },
    { id: 2, name: 'Institute 2' },
    { id: 3, name: 'Institute 3' },
    { id: 4, name: 'Institute 4' },
];
instituteCam = [
    { id: 1, name: 'Institute Campus 1'},
    { id: 2, name: 'Institute Campus 2' },
   
];
}
