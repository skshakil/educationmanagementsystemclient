import { Component, OnInit, ViewChild } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AuthService } from 'auth-library';
import { ToastrService } from 'ngx-toastr';
import { DeleteCampusComponent } from './delete-campus/delete-campus.component';
import { AddCampusComponent } from './add-campus/add-campus.component';
@Component({
  selector: 'app-campus-list',
  templateUrl: './campus-list.component.html',
  styleUrls: ['./campus-list.component.css']
})
export class CampusListComponent implements OnInit {

  showSuccess() {
    this.toastr.success('Toastr Working');
  }

@ViewChild('institutegridTable', { static: true }) institutegridTable : any;
campusModalRef: BsModalRef | undefined;
deleteModalRef: BsModalRef | undefined;
instituteList: any;
institutegridTableObj: any;
  selectedStObj: any;

  constructor(
    private modalService :BsModalService,
    private authService: AuthService,
     private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.initInstituteGrid();

     
  }

  initInstituteGrid() {
    let that = this;
    this.instituteList = $(this.institutegridTable.nativeElement);
    this.institutegridTableObj = this.instituteList.DataTable({
      pagingType: 'full_numbers',
      pageLength: 10,
      serverSide: true,
      processing: true,
      ajax: {
        url:
          environment.baseUrl + environment.authApiUrl + '/campus/gridList',
        type: 'GET',
        data: function (sendData: any) {},
        // beforeSend: function (xhr: any) {
        //   xhr.setRequestHeader(
        //     'Authorization',
        //     'bearer ' + 'd12aec46-f31f-4b73-ab7a-312fd1c7a8f4'
        //   );
        //   xhr.setRequestHeader('Content-Type', 'application/json');
        // },
        beforeSend: function (xhr : any) {
          xhr.setRequestHeader(
            'Authorization',
            'bearer ' + that.authService.getAccessToken()
          );
          xhr.setRequestHeader('Content-Type', 'application/json');
        },
        dataSrc: function (response: any) {
          response.draw = response.obj.draw;
          console.log('Grid: ', response);
          response.recordsTotal = response.obj.recordsTotal;
          response.recordsFiltered = response.obj.recordsFiltered;
          return response.obj.data;
        },
        error: function (request: any) {
          console.log('request.responseText', request.responseText);
        },
      },
      order: [[0, 'desc']],
      columns: [
        {
          width: '100px',
          title: 'ID',
          data: 'instituteId',
          name: 'instituteId',
        },
        
        {
          title: 'Institute Name',
          data: 'InstituteNmae',
          name: 'InstituteNmae',
        },
        {
          title: 'Institute Campus',
          data: 'InstituteCampus',
          name: 'InstituteCampus',
        },
        {
          title: 'Address',
           data: 'Address',
          name: 'Address',
        },
         {
          title: 'Contact',
           data: 'Contact',
          name: 'Contact',
        },
         {
          title: 'Order',
          data: 'Order',
          name: 'Order',
        },
        {
          title: 'Status',
          data: 'Status',
          name: 'Status',
        },
        {
          className: 'text-center',
          title: 'Action',
          data: '',
          render: (data: any, type: any, full: any) => {
            return '<button class= "ptn btn-info table-btn-all" type="button"><i class="far fa-edit"></i></button> <button class= "ptn btn-info table-btn-all" type="button"><i class="far fa-trash-alt"></i></button>'
          }
        }
      ],
      responsive: true,
      select: true,
      rowCallback: (row: Node, data: any[] | Object) => {
        $(row).unbind('click');
        $(row).bind('click', () => {
          $(row).removeClass('selected');
          $(row).addClass('selected');
          this.onClickGridData(data);
        });
        return row;
      },
    });
  }

  onClickGridData(data: any): void {
    console.log(data);
    this.selectedStObj = data;
  }

 
  addCampusModal() {
    this.campusModalRef = this.modalService.show(AddCampusComponent, {class: 'modal-lg'});
  }
  deleteTeacherModal() {
    this.deleteModalRef = this.modalService.show(DeleteCampusComponent, {class: 'modal-sm'});
  }
}

