import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-delete-campus',
  templateUrl: './delete-campus.component.html',
  styleUrls: ['./delete-campus.component.css']
})
export class DeleteCampusComponent implements OnInit {

  constructor(
    public deleteCampusModal: BsModalRef,
  ) { }

  ngOnInit(): void {
  }

}
