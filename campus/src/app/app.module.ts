import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { DataTablesModule } from 'angular-datatables';
import { ModalModule, BsModalRef } from 'ngx-bootstrap/modal';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  AuthGuard,
  AuthLibraryModule,
  AuthService,
  TokenInterceptorService,
} from 'auth-library';
import { CoreLibraryModule } from 'core-library';
import { environment } from 'src/environments/environment';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CampusListComponent } from './campus-list/campus-list.component';
import { AddCampusComponent } from './campus-list/add-campus/add-campus.component';
import { DeleteCampusComponent } from './campus-list/delete-campus/delete-campus.component';
@NgModule({
  declarations: [
    AppComponent,
    CampusListComponent,
    AddCampusComponent,
    DeleteCampusComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    NgSelectModule,
    DataTablesModule,
    AuthLibraryModule.forRoot(environment),
    CoreLibraryModule.forRoot(environment),
    BsDropdownModule.forRoot(),
    ToastrModule.forRoot(),
  ],
  providers: [
    AuthGuard,
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
  ],
  entryComponents: [
    AddCampusComponent,
    DeleteCampusComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  ModalModule: any;
  DataTablesModule: any
}
