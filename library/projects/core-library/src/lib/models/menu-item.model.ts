import { Resource } from "../resource/models/resource.model";

export class MenuItemModel extends Resource {
    displayName: string;
    route: string;
    iconName: string;
    children?: MenuItemModel[];
}
