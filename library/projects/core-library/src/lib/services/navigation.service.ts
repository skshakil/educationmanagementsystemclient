import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MenuItemModel } from '../models/menu-item.model';
import { ResourceService } from '../resource/services/resource.service';
import { MenuItemSerializer } from '../serializers/menu-item.serializer';

@Injectable({
  providedIn: 'root'
})
export class NavigationService extends ResourceService<MenuItemModel>{

  private END_POINT = `api/coreUser`;
  private MENU_LIST_AUTH = `${this.environment.baseUrl}${this.environment.authApiUrl}/${this.END_POINT}/find-user-menu`;
  // private MENU_LIST_JSON = `./assets/json/nevigation-data.json`;

  constructor(@Inject('env') public environment, httpClient: HttpClient, private http: HttpClient) {
    super(environment,httpClient, environment.authApiUrl, `api/coreUser`, new MenuItemSerializer());
  }

  getAuthMenuList(): Observable<any> {
    return this.http.get<any>(this.MENU_LIST_AUTH).pipe(
      map((data: any) => data
    ));
  } 

}
