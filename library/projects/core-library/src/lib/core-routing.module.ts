import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const basePath = 'educare';
const routes: Routes = [
  /**Example Route */
  /* {
    path: basePath+'/login', 
    component:AuthComponent
  } */
]  

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class CoreRoutingModule { }
