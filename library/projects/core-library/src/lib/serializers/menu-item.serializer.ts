import { MenuItemModel } from "../models/menu-item.model";

export class MenuItemSerializer {
    
    fromJson(json: any): MenuItemModel {
        const menuItemModel = new MenuItemModel();
        menuItemModel.displayName = json.displayName;
        menuItemModel.route = json.route;
        menuItemModel.iconName = json.iconName;
        menuItemModel.children = json.children;

        return menuItemModel;
    }

    toJson(menuItemModel: MenuItemModel): any {
        return {
            displayName : menuItemModel.displayName,
            route       : menuItemModel.route,
            iconName    : menuItemModel.iconName,
            children    : menuItemModel.children
        };
    }
}
