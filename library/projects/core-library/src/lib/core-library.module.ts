import { NgModule } from '@angular/core';
import { CoreRoutingModule } from './core-routing.module';
import { HomeLayoutComponent } from './layouts/home-layout/home-layout.component';
import { DashboardLayoutComponent } from './layouts/dashboard-layout/dashboard-layout.component';
import { NavigationComponent } from './navigation/navigation.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SubHeaderComponent } from './sub-header/sub-header.component';
import { MenuItemComponent } from './navigation/menu-item/menu-item.component';
import { BrowserModule } from '@angular/platform-browser';
import { GlobalDashboardComponent } from './global-dashboard/global-dashboard.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
@NgModule({
  declarations: [
    HomeLayoutComponent, 
    DashboardLayoutComponent, 
    NavigationComponent, 
    HeaderComponent, 
    FooterComponent, 
    SubHeaderComponent, 
    MenuItemComponent, 
    GlobalDashboardComponent
  ],
  imports: [
    BrowserModule,
    CoreRoutingModule,
    BsDropdownModule.forRoot()

  ],
  exports: [HomeLayoutComponent]
})
export class CoreLibraryModule {
  public static forRoot(environment: any) {
    return {
        ngModule: CoreLibraryModule,
        providers: [ 
          {
            provide: 'env',
            useValue: environment
          }
        ]
    };
  }
}
