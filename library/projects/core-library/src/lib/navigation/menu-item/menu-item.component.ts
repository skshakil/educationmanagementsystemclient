import { Component, Inject, Input, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'lib-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.css']
})
export class MenuItemComponent implements OnInit {
  basePath : string;
  @Input() navMenuList: [];


  constructor(  
      @Inject('env') private environment,
  
) { 
  this.basePath = this.environment.basePath;
}


  ngOnInit(): void {
    $(document).ready(function() {
      $('.sidebar-dropdown > a').click(function (this:any) {
       // this:any, target: Object, propertyName: string
       $('.sidebar-submenu').slideUp(200);
       if ($(this).parent().hasClass('active')) {
           $('.sidebar-dropdown').removeClass('active');
           $(this).parent().removeClass('active');
       } else {
           $('.sidebar-dropdown').removeClass('active');
           $(this).next('.sidebar-submenu').slideDown(200);
           $(this).parent().addClass('active');
       }
   });
   $('#close-sidebar').click(function () {
       $('.page-wrapper').removeClass('toggled');
   });
   $('#show-sidebar').click(function () {
       $('.page-wrapper').addClass('toggled');
   });
});
  }
 
}
