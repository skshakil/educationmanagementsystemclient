import { Component, Inject, OnInit } from '@angular/core'; 
import { MenuItemModel } from '../models/menu-item.model';
import { NavigationService } from '../services/navigation.service';
import { AuthService } from 'auth-library';

@Component({
  selector: 'lib-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  basePath : string;
  navMenuList : MenuItemModel[] = [];
  
  constructor(
    @Inject('env') private environment,
    private navigationService: NavigationService,
    private authService: AuthService
  ) { 
    this.basePath = this.environment.basePath;
  }

  ngOnInit(): void {
    this.getAuthMenuList()
  }
  getAuthMenuList(){
    this.navigationService.getAuthMenuList().subscribe(res=>{
      this.generateMenuTree(res.items);
    },err=>{
      console.log('Navigation Error:=>',err);
    })
  }
  generateMenuTree(menuList: any[]) {
    let roots = [];
    let nodes = [];
    let dispvaal = [];
    let j = 0;
    for (let i in menuList) {
      let menu = menuList[i];
      dispvaal[j] = menu.pageLink;
      j++;
      nodes[menu.childId] = { 
        displayName: menu.displayValue, 
        childId: menu.childId,
        route: menu.pageLink,
        iconName:menu.iconName,
        children: []
      };
      if (menu.parentId == 0) {
        roots.push(nodes[menu.childId]);
      }
      else {
        nodes[menu.parentId].children.push(nodes[menu.childId]);
      }
    }
    this.navMenuList = roots;
  }


  onLogout() {
    this.authService.logout();
  }
  
}
