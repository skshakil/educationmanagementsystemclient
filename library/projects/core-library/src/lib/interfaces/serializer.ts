import { Resource } from "../resource/models/resource.model";

export interface Serializer {
    fromJson(json: any): Resource;
    toJson(resource: Resource): any;
}
