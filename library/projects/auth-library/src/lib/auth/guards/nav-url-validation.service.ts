import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NavUrlValidationService {
  private END_POINT = `api/coreUser`;
  private MENU_LIST_AUTH = `${this.environment.baseUrl}${this.environment.authApiUrl}/${this.END_POINT}/find-user-menu`;

  bsNavObject = new BehaviorSubject<any>(null);
  isUrlValidObj: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
   
  constructor(
    @Inject('env') public environment, 
    private http: HttpClient
  ) {}

  getAuthMenuList(): Observable<any> {
    return this.http.get<any>(this.MENU_LIST_AUTH).pipe(
      map((data: any) => data
    ));
  }  

  getNavItemBehaviorObject(): Observable<boolean> {
    return this.bsNavObject.asObservable();
  }

  setNavItemBehaviorObject(navItems) {
    this.bsNavObject.next(navItems);
  }
  



  // isPageLinkValid(pageLink:any){
  //   this.linkValidation(pageLink);
  //   return this.isUrlValid.asObservable();
  // }

  // linkValidation(pageLink:string){

  //   pageLink =  pageLink.substring(9);

  //   let that = this;
  //   this.navBsSubject.subscribe(value=>{
  //     console.log('PageLink:=>',pageLink,'BS Value:=> ', value); 
  //     if(value && value.length>0){
  //       if(that.findPageLink(value,pageLink)){
  //         return true;
  //       }
  //       return false;
  //     }else{
  //       that.getAuthMenuList().subscribe(
  //         resp=>{
  //           console.log('Auth Menu:=. ', resp);
  //           if(resp.items && resp.items.length>0){
  //             that.navBsSubject.next(resp.items);
  //             if(that.findPageLink(resp.items,pageLink)){
  //               that.isUrlValid.next(true);
  //             }else{
  //               that.isUrlValid.next(false);
  //             }
  //           }else{
  //             that.navBsSubject.next([]);
  //             that.isUrlValid.next(false);
  //           }  
  //         },
  //         err=>{
  //           that.navBsSubject.next([]);
  //           that.isUrlValid.next(false);
  //         } 
  //       )
  //     }
  //   }) 
  // }


  // findPageLink(items:any[],pageLink:string){
  //   let getLink = items.filter(x => x.pageLink?((x.pageLink).toUpperCase()).trim():'' === pageLink?(pageLink.toUpperCase()).trim():'');
  //   if(getLink && getLink.length>0){
  //     return true;
  //   }else{
  //     return false;
  //   }
  // }

}

