import { Inject, Injectable } from '@angular/core';
import { 
  CanActivate,
  ActivatedRouteSnapshot, 
  RouterStateSnapshot, 
  Router 
} from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthService } from '../service/auth.service';
import { map, take } from 'rxjs/operators';
import { NavUrlValidationService } from './nav-url-validation.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private authService: AuthService, 
        private router: Router,
        @Inject('env') private env,
        private navUrlValidationService :NavUrlValidationService
    ) { }

    bsPageLinkValidation = new BehaviorSubject<boolean>(false);

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.authService.isLoggedIn.pipe(
            take(1),
            map((isLoggedIn: boolean) => {
                if (!isLoggedIn) {
                    this.router.navigate([this.env.basePath+'/login']);
                    return false;
                }
                // PageLink Validation
                // if(!this.navUrlValidationService.isPageLinkValid(state.url)){
                //     this.router.navigate([this.env.basePath+'/unauthorize']);
                //     return false;
                // }

                return true;
            })
        );
    }
}
