import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../service/auth.service';

@Component({
  selector: 'lib-login-global',
  templateUrl: './login-global.component.html',
  styleUrls: ['./login-global.component.css']
})
export class LoginGlobalComponent implements OnInit {

  userName : string = null;
  password : string = null;

  errorMgs : any;

  constructor(
    @Inject('env') private environment,
    private router : Router,
    private authService : AuthService
  ) { }

  ngOnInit(): void {
    this.isMessStatus();
  }

  login(){
    console.log(this.environment);
      // this.router.navigate([this.environment.basePath+'/'+this.environment.apiPath+'/dashboard']);
    if(this.userName && this.password){
      const userObj = {
        userName: this.userName,
        password: this.password
      }
     // console.log(userObj);
      this.authService.obtainAccessToken(userObj); 
    }else{
      alert('Enter Username & Password')
    }
    

  }

  isMessStatus(): void {
    this.authService.messStatus().subscribe(errorMgs => this.errorMgs = errorMgs);
  }

  // redirectTo(){
  //   this.router.navigate([this.environment.basePath+'/'+this.environment.apiPath+'/dashboard']);
  // }

}
