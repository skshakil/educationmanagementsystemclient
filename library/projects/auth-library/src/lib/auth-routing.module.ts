import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'; 
import { AuthComponent } from './auth/auth.component';
import { NotFoundComponent } from './auth/not-found/not-found.component';
import { SignupGlobalComponent } from './auth/signup/signup-global/signup-global.component';
import { UnauthorizeComponent } from './auth/unauthorize/unauthorize.component';

const basePath = 'educare';
const routes: Routes = [
  {
    path: basePath+'/login', 
    component:AuthComponent
  },
  {
    path: 'signup', 
    component:SignupGlobalComponent
  }, 
  { path: basePath+'/unauthorize', component: UnauthorizeComponent },
  { path: '**', component: NotFoundComponent }
  
  // {
  //   path: '', 
  //   // component: AuthComponent,
  //   children: [
  //     {
  //       path: '404',
  //       component: NotFoundComponent
  //     }
  //   ]
  // },
  // {path: '**', redirectTo: '/404'}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [{ provide: LocationStrategy, useClass: PathLocationStrategy }]
})
export class AuthRoutingModule { }
