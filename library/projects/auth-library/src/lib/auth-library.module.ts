import { NgModule } from '@angular/core';
import { AuthRoutingModule } from './auth-routing.module';
import { AuthComponent } from './auth/auth.component';
import { LoginCommonComponent } from './auth/login/login-common/login-common.component';
import { SignupGlobalComponent } from './auth/signup/signup-global/signup-global.component';
import { LoginGlobalComponent } from './auth/login/login-global/login-global.component';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptorService } from './auth/service/token-interceptor.service';
import { NotFoundComponent } from './auth/not-found/not-found.component';
import { UnauthorizeComponent } from './auth/unauthorize/unauthorize.component';

@NgModule({
  declarations: [
    AuthComponent, 
    LoginCommonComponent, 
    SignupGlobalComponent, 
    LoginGlobalComponent, 
    NotFoundComponent, 
    UnauthorizeComponent
  ],
  imports: [
    FormsModule,
    AuthRoutingModule
  ],
  exports: [],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
})
export class AuthLibraryModule { 
  public static forRoot(environment: any) {
    return {
        ngModule: AuthLibraryModule,
        providers: [ 
          {
            provide: 'env',
            useValue: environment
          }
        ]
    };
  }
}
