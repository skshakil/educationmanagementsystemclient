import { TestBed } from '@angular/core/testing';

import { UtilLibraryService } from './util-library.service';

describe('UtilLibraryService', () => {
  let service: UtilLibraryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UtilLibraryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
