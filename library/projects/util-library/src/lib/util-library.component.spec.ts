import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UtilLibraryComponent } from './util-library.component';

describe('UtilLibraryComponent', () => {
  let component: UtilLibraryComponent;
  let fixture: ComponentFixture<UtilLibraryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UtilLibraryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UtilLibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
