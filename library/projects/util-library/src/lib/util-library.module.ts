import { NgModule } from '@angular/core';
import { UtilLibraryComponent } from './util-library.component';



@NgModule({
  declarations: [UtilLibraryComponent],
  imports: [
  ],
  exports: [UtilLibraryComponent]
})
export class UtilLibraryModule { }
