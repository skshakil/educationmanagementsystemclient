import { Component, OnInit } from '@angular/core';
import { assetUrl } from 'src/single-spa/asset-url';
@Component({
  selector: 'app-student-document',
  templateUrl: './student-document.component.html',
  styleUrls: ['./student-document.component.css']
})

export class StudentDocumentComponent implements OnInit {
  sigAvatar : any;
  constructor() { }

  ngOnInit(): void {
    this.sigAvatar = assetUrl('images/sig.jpg');
  }

}
