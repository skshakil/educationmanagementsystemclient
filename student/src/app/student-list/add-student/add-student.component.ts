import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, Output, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { StudentEducationEntity } from 'src/app/_coreAddStudent/model/student-education-entity';
import { StudentFamilyEntity } from 'src/app/_coreAddStudent/model/student-family-entity';
import { assetUrl } from 'src/single-spa/asset-url';
import { StudentEntity } from 'src/app/_coreAddStudent/model/student-entity';
import { StudentService } from 'src/app/_coreAddStudent/services/student-service.service';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css'],
})
export class AddStudentComponent implements OnInit {
  @ViewChild('tabset') tabset: TabsetComponent | undefined;
  // tab
  tabs: any[] = [
    {
      title: 'Perosnal Details',
      content: 'PerosnalDetails',
      initiated: true,
      active: true,
    },
    { title: 'Family', content: 'family', initiated: true },
    { title: 'Education', content: 'Education', initiated: true },
    { title: 'Document', content: 'Document', initiated: true },
    { title: 'Experience', content: 'Experience', initiated: true },
    { title: 'Achievement', content: 'Achievement', initiated: true },
    { title: 'Award', content: 'Award', initiated: true },
    { title: 'Training', content: 'Training', initiated: true },
    { title: 'Signature', content: 'Signature', initiated: true },
  ];

  profileAvatar: any;
  addStudentFormGroup: FormGroup;
  studentEntity: StudentEntity;
  studentFamilyEntity: StudentFamilyEntity = null;
  studentEducationEntity: StudentEducationEntity = null;
  preAddress: string;
  perAddress: string;

  constructor(
    public addStudentModalRef: BsModalRef,
    public formBuilder: FormBuilder,
    private studentService: StudentService
  ) {}

  ngOnInit(): void {
    this.addStudentFormObj();
    this.profileAvatar = assetUrl('images/profile-placeholder.jpg');
  }

  addStudentFormObj(): void {
    this.addStudentFormGroup = this.formBuilder.group({
      studentObj: this.formBuilder.group({
        studentId: new FormControl(''),
        salutation: new FormControl(''),
        stFName: new FormControl(''),
        stMName: new FormControl(''),
        stLName: new FormControl(''),
        fatherName: new FormControl(''),
        motherName: new FormControl(''),
        gender: new FormControl(''),
        nationality: new FormControl(''),
        spouseeName: new FormControl(''),
        dob: new FormControl(''),
      }),
    });
  }

  get studentId() {
    return this.addStudentFormGroup.get('studentObj.studentId');
  }
  get salutation() {
    return this.addStudentFormGroup.get('studentObj.salutation');
  }
  get stFName() {
    return this.addStudentFormGroup.get('studentObj.stFName');
  }
  get stLName() {
    return this.addStudentFormGroup.get('studentObj.stMName');
  }
  get stMName() {
    return this.addStudentFormGroup.get('studentObj.stLName');
  }
  get fatherName() {
    return this.addStudentFormGroup.get('studentObj.fatherName');
  }
  get motherName() {
    return this.addStudentFormGroup.get('studentObj.motherName');
  }
  get gender() {
    return this.addStudentFormGroup.get('studentObj.gender');
  }
  get nationality() {
    return this.addStudentFormGroup.get('studentObj.nationality');
  }
  get spouseeName() {
    return this.addStudentFormGroup.get('studentObj.spouseeName');
  }
  get dob() {
    return this.addStudentFormGroup.get('studentObj.dob');
  }

  saveStudent() {
    if (this.addStudentFormGroup.invalid) {
      this.addStudentFormGroup.markAllAsTouched();
      return;
    }

    this.studentEntity = this.addStudentFormGroup.controls['studentObj'].value;
    this.studentEntity.presentAddress = this.preAddress;
    this.studentEntity.permanentAddress = this.perAddress;

    if (this.studentFamilyEntity) {
      this.studentFamilyEntity.studentNo = this.studentEntity.studentId;
    }
    if (this.studentEducationEntity) {
      this.studentEducationEntity.studentNo = this.studentEntity.studentId;
    }

    let studentObjValue = {
      studentObj: this.studentEntity,
      familyInfoList: [this.studentFamilyEntity],
      educationalInfoList: [this.studentEducationEntity],
    };
    console.log(studentObjValue);
    this.studentService.addStudent(studentObjValue).subscribe(
      (data) => {
        console.log('success');
        window.location.reload();
      },
      (err) => {
        console.log('error');
      }
    );
  }

  receiveFamilyInfoData($event) {
    this.studentFamilyEntity = $event;
  }

  receiveEducationInfoData($event) {
    this.studentEducationEntity = $event;
  }

  receivePersonalInfoData($event) {
    this.preAddress = $event[0];
    this.perAddress = $event[1];
  }

  onSelect(data: any): void {
    console.log('Selected Tabset tabs:', data);
  }
}
