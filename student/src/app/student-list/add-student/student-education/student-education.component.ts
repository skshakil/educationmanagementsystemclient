import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { StudentEducationEntity } from 'src/app/_coreAddStudent/model/student-education-entity';

@Component({
  selector: 'app-student-education',
  templateUrl: './student-education.component.html',
  styleUrls: ['./student-education.component.css'],
})
export class StudentEducationComponent implements OnInit {
  @Output() event = new EventEmitter<StudentEducationEntity>();

  studentEducationFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.studentEducationFormObj();
  }

  studentEducationFormObj() {
    this.studentEducationFormGroup = this.formBuilder.group({
      studentEducationInfo: this.formBuilder.group({
        educationId: new FormControl(''),
        studentNo: new FormControl(''),
        courseName: new FormControl(''),
        admissionYear: new FormControl(''),
        institutionName: new FormControl(''),
        mejorSubject: new FormControl(''),
        boardName: new FormControl(''),
      }),
    });
  }

  get educationId() {
    return this.studentEducationFormGroup.get(
      'studentEducationInfo.educationId'
    );
  }
  get studentNo() {
    return this.studentEducationFormGroup.get('studentEducationInfo.studentNo');
  }
  get courseName() {
    return this.studentEducationFormGroup.get(
      'studentEducationInfo.courseName'
    );
  }
  get admissionYear() {
    return this.studentEducationFormGroup.get(
      'studentEducationInfo.admissionYear'
    );
  }
  get institutionName() {
    return this.studentEducationFormGroup.get(
      'studentEducationInfo.institutionName'
    );
  }
  get mejorSubject() {
    return this.studentEducationFormGroup.get(
      'studentEducationInfo.mejorSubject'
    );
  }
  get boardName() {
    return this.studentEducationFormGroup.get('studentEducationInfo.boardName');
  }

  saveStudentEducation() {
    const educationInfo =
      this.studentEducationFormGroup.controls['studentEducationInfo'].value;
    this.event.emit(educationInfo);
  }
}
