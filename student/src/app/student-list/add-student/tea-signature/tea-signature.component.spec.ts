import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeaSignatureComponent } from './tea-signature.component';

describe('TeaSignatureComponent', () => {
  let component: TeaSignatureComponent;
  let fixture: ComponentFixture<TeaSignatureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeaSignatureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeaSignatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
