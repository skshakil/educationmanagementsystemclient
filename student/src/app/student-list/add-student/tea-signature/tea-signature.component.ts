import { Component, OnInit } from '@angular/core';
import { assetUrl } from 'src/single-spa/asset-url';

@Component({
  selector: 'app-tea-signature',
  templateUrl: './tea-signature.component.html',
  styleUrls: ['./tea-signature.component.css']
})
export class TeaSignatureComponent implements OnInit {
  sigAvatar : any;
  constructor() { }

  ngOnInit(): void {
    this.sigAvatar = assetUrl('images/sig.jpg');
  }

}
