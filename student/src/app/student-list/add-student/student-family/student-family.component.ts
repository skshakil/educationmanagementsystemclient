import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { StudentFamilyEntity } from 'src/app/_coreAddStudent/model/student-family-entity';

@Component({
  selector: 'app-student-family',
  templateUrl: './student-family.component.html',
  styleUrls: ['./student-family.component.css'],
})
export class StudentFamilyComponent implements OnInit {
  @Output() event = new EventEmitter<StudentFamilyEntity>();

  studentFamilyFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.studentFamilyFormObj();
  }

  studentFamilyFormObj() {
    this.studentFamilyFormGroup = this.formBuilder.group({
      studentFamilyInfo: this.formBuilder.group({
        studentFamilyId: new FormControl(''),
        studentNo: new FormControl(''),
        memberName: new FormControl(''),
        relationNo: new FormControl(''),
        mobileNo: new FormControl(''),
        address: new FormControl(''),
      }),
    });
  }

  get studentFamilyId() {
    return this.studentFamilyFormGroup.get('studentFamilyInfo.studentFamilyId');
  }
  get studentNo() {
    return this.studentFamilyFormGroup.get('studentFamilyInfo.studentNo');
  }
  get memberName() {
    return this.studentFamilyFormGroup.get('studentFamilyInfo.memberName');
  }
  get relationNo() {
    return this.studentFamilyFormGroup.get('studentFamilyInfo.relationNo');
  }
  get mobileNo() {
    return this.studentFamilyFormGroup.get('studentFamilyInfo.mobileNo');
  }
  get address() {
    return this.studentFamilyFormGroup.get('studentFamilyInfo.address');
  }

  saveStudentFamilyInfo() {
    const studentFamilyInfo =
      this.studentFamilyFormGroup.controls['studentFamilyInfo'].value;
    this.event.emit(studentFamilyInfo);
  }
}
