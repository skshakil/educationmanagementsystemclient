import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { PersonalInfo } from 'src/app/_coreAddStudent/model/personal-info';

@Component({
  selector: 'app-persona-details',
  templateUrl: './persona-details.component.html',
  styleUrls: ['./persona-details.component.css'],
})
export class PersonaDetailsComponent implements OnInit {
  personalInfoFormGroup: FormGroup;
  personalEntity: PersonalInfo;
  presentAddress: string;
  permanentAddress: string;

  constructor(private formBuilder: FormBuilder) {}

  @Output() event = new EventEmitter<string[]>();

  ngOnInit(): void {
    this.personalInfoFormObj();
  }

  personalInfoFormObj() {
    this.personalInfoFormGroup = this.formBuilder.group({
      presentAddress: this.formBuilder.group({
        street: new FormControl(''),
        village: new FormControl(''),
        union: new FormControl(''),
        postOffice: new FormControl(''),
        thana: new FormControl(''),
        district: new FormControl(''),
        country: new FormControl(''),
      }),
      permanentAddress: this.formBuilder.group({
        street: new FormControl(''),
        village: new FormControl(''),
        union: new FormControl(''),
        postOffice: new FormControl(''),
        thana: new FormControl(''),
        district: new FormControl(''),
        country: new FormControl(''),
      }),
    });
  }

  get presentStreet() {
    return this.personalInfoFormGroup.get('presentAddress.street');
  }
  get presentVillage() {
    return this.personalInfoFormGroup.get('presentAddress.village');
  }
  get presentUnion() {
    return this.personalInfoFormGroup.get('presentAddress.union');
  }
  get presentPostOffice() {
    return this.personalInfoFormGroup.get('presentAddress.postOffice');
  }
  get presentThana() {
    return this.personalInfoFormGroup.get('presentAddress.thana');
  }
  get presentDistrict() {
    return this.personalInfoFormGroup.get('presentAddress.district');
  }
  get presentCountry() {
    return this.personalInfoFormGroup.get('presentAddress.country');
  }

  get permanentStreet() {
    return this.personalInfoFormGroup.get('permanentAddress.street');
  }
  get permanentVillage() {
    return this.personalInfoFormGroup.get('permanentAddress.village');
  }
  get permanentUnion() {
    return this.personalInfoFormGroup.get('permanentAddress.union');
  }
  get permanentPostOffice() {
    return this.personalInfoFormGroup.get('permanentAddress.postOffice');
  }
  get permanentThana() {
    return this.personalInfoFormGroup.get('permanentAddress.thana');
  }
  get permanentDistrict() {
    return this.personalInfoFormGroup.get('permanentAddress.district');
  }
  get permanentCountry() {
    return this.personalInfoFormGroup.get('permanentAddress.country');
  }

  savePersonalInfo() {
    // this.presentAddress = this.personalInfoFormGroup.controls.presentAddress.value;
    // this.permanentAddress = this.personalInfoFormGroup.controls.permanentAddress.value;
    this.presentAddress =
      this.presentStreet.value +
      ',' +
      this.presentVillage.value +
      ',' +
      this.presentUnion.value +
      ',' +
      this.presentPostOffice.value +
      ',' +
      this.presentThana.value +
      ',' +
      this.presentDistrict.value +
      ',' +
      this.presentCountry.value;
    this.permanentAddress =
      this.permanentStreet.value +
      ',' +
      this.permanentVillage.value +
      ',' +
      this.permanentUnion.value +
      ',' +
      this.permanentPostOffice.value +
      ',' +
      this.permanentThana.value +
      ',' +
      this.permanentDistrict.value +
      ',' +
      this.permanentCountry.value;

    this.event.emit([this.presentAddress, this.permanentAddress]);
  }

  copyPresentAddressToPermanentAddress($event) {
    if ($event.target.checked) {
      this.personalInfoFormGroup.controls.permanentAddress.setValue(
        this.personalInfoFormGroup.controls.presentAddress.value
      );
    } else {
      this.personalInfoFormGroup.controls.permanentAddress.reset();
    }
  }
}
