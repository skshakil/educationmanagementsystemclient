import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { environment } from 'src/environments/environment';
import { AddStudentComponent } from './add-student/add-student.component';
import { AuthService } from 'auth-library';
import * as moment from 'moment';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css'],
})
export class StudentListComponent implements OnInit {
  

  @ViewChild('studentgridList', { static: true }) studentgridTable;
  addStudentModalRef: BsModalRef | undefined;
  studentList: any;
  studentgridTableObj: any;
  selectedStObj: any;

  constructor(
    private modalService: BsModalService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.initStudentGrid();

     console.log('Moment Check',moment(new Date()).format("DD-MM-YYYY"));
     
  }

  initStudentGrid() {
    let that = this;
    this.studentList = $(this.studentgridTable.nativeElement);
    this.studentgridTableObj = this.studentList.DataTable({
      pagingType: 'full_numbers',
      pageLength: 10,
      serverSide: true,
      processing: true,
      ajax: {
        url:
          environment.baseUrl + environment.studentApiUrl + '/student/gridList',
        type: 'GET',
        data: function (sendData: any) {},
        // beforeSend: function (xhr: any) {
        //   xhr.setRequestHeader(
        //     'Authorization',
        //     'bearer ' + 'd12aec46-f31f-4b73-ab7a-312fd1c7a8f4'
        //   );
        //   xhr.setRequestHeader('Content-Type', 'application/json');
        // },
        beforeSend: function (xhr) {
          xhr.setRequestHeader(
            'Authorization',
            'bearer ' + that.authService.getAccessToken()
          );
          xhr.setRequestHeader('Content-Type', 'application/json');
        },
        dataSrc: function (response: any) {
          response.draw = response.obj.draw;
          console.log('Grid: ', response);
          response.recordsTotal = response.obj.recordsTotal;
          response.recordsFiltered = response.obj.recordsFiltered;
          return response.obj.data;
        },
        error: function (request: any) {
          console.log('request.responseText', request.responseText);
        },
      },
      order: [[0, 'desc']],
      columns: [
        {
          width: '100px',
          title: 'ID',
          data: 'studentId',
          name: 'studentId',
        },
        {
          title: 'Name',
          data: '',
          name: 'stFName',
          render: (data, type, full) => {
            return full.salutation + ' ' + full.stFName + ' ' + full.stLName;
          },
        },
        {
          width: '100px',
          title: 'Gender',
          data: 'gender',
        },
        {
          title: 'Father Name',
          data: 'fatherName',
        },
        {
          title: 'Nationality',
          data: 'nationality',
        },
      ],
      responsive: true,
      select: true,
      rowCallback: (row: Node, data: any[] | Object) => {
        $(row).unbind('click');
        $(row).bind('click', () => {
          $(row).removeClass('selected');
          $(row).addClass('selected');
          this.onClickGridData(data);
        });
        return row;
      },
    });
  }

  onClickGridData(data: any): void {
    console.log(data);
    this.selectedStObj = data;
  }

  deleteStudent(): void {
    if (!this.selectedStObj) {
      console.log('Pleae select a student !');
      return;
    }
    console.log('Student Found ID:', this.selectedStObj.id);
  }

  addStudentModal() {
    this.addStudentModalRef = this.modalService.show(AddStudentComponent, {
      class: 'modal-lg',
    });
  }
  
}
