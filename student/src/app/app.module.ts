import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { StudentListComponent } from './student-list/student-list.component';
import { DataTablesModule } from 'angular-datatables';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule, BsModalRef } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import {
  AuthGuard,
  AuthLibraryModule,
  AuthService,
  TokenInterceptorService,
} from 'auth-library';
import { CoreLibraryModule } from 'core-library';
import { AddStudentComponent } from './student-list/add-student/add-student.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { PersonaDetailsComponent } from './student-list/add-student/persona-details/persona-details.component';
import { StudentFamilyComponent } from './student-list/add-student/student-family/student-family.component';
import { StudentEducationComponent } from './student-list/add-student/student-education/student-education.component';
import { StudentDocumentComponent } from './student-list/add-student/student-document/student-document.component';
import { StudentExperienceComponent } from './student-list/add-student/student-experience/student-experience.component';
import { StudentAchievementComponent } from './student-list/add-student/student-achievement/student-achievement.component';
import { StudentAwardComponent } from './student-list/add-student/student-award/student-award.component';
import { StudentTrainingComponent } from './student-list/add-student/student-training/student-training.component';
import { TeaSignatureComponent } from './student-list/add-student/tea-signature/tea-signature.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    StudentListComponent,
    AddStudentComponent,
    PersonaDetailsComponent,
    StudentFamilyComponent,
    StudentEducationComponent,
    StudentDocumentComponent,
    StudentExperienceComponent,
    StudentAchievementComponent,
    StudentAwardComponent,
    StudentTrainingComponent,
    TeaSignatureComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    DataTablesModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    ModalModule,
    AccordionModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    AuthLibraryModule.forRoot(environment),
    CoreLibraryModule.forRoot(environment),
    TooltipModule.forRoot(),
    TypeaheadModule.forRoot(),
    NgSelectModule,
    ReactiveFormsModule,
  ],
  entryComponents: [AddStudentComponent],
  providers: [
    AuthGuard,
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  DataTablesModule: any;
  BsDatepickerModule: any;
  TabsModule: any;
  ModalModule: any;
  TypeaheadModule: any;
  AccordionModule: any;
}
