export class StudentEntity {
  studentId: number;
  salutation: string;
  stFName: string;
  stMName: string;
  stLName: string;
  fatherName: string;
  motherName: string;
  gerder: string;
  nationality: string;
  spouseeName: string;
  dob: Date;

  presentAddress: any;
  permanentAddress: any;
}
