export class StudentFamilyEntity {
  studentFamilyId: number;
  studentNo: number;
  memberName: string;
  relationNo: number;
  mobileNo: string;
  address: string;
}
