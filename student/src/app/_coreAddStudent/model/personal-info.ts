export class PersonalInfo {
  street: string;
  village: string;
  union: string;
  postOffice: string;
  thana: string;
  district: string;
  country: string;
}
