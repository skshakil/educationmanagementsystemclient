export class StudentEducationEntity {
  educationId: number;
  studentNo: number;
  courseName: string;
  admissionYear: number;
  institutionName: string;
  mejorSubject: string;
  boardName: string;
}
