import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StudentEntity } from '../model/student-entity';
import { StudentFamilyEntity } from '../model/student-family-entity';

@Injectable({
  providedIn: 'root',
})
export class StudentService {
  constructor(private http: HttpClient) {}

  addStudent(studentEntity: any): Observable<any> {
    return this.http.post(
      environment.baseUrl + environment.studentApiUrl + '/student/save-info',
      studentEntity
    );
  }
}
