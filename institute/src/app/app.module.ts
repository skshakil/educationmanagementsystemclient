import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToastrModule } from 'ngx-toastr';
import { DataTablesModule } from 'angular-datatables';
import { ModalModule, BsModalRef } from 'ngx-bootstrap/modal';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';

import {
  AuthGuard,
  AuthLibraryModule,
  AuthService,
  TokenInterceptorService,
} from 'auth-library';
import { CoreLibraryModule } from 'core-library';
import { environment } from 'src/environments/environment';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { InstituteListComponent } from './institute-list/institute-list.component';
import { AddInstitueComponent } from './institute-list/add-institue/add-institue.component';
import { DeleteInstituteComponent } from './institute-list/delete-institute/delete-institute.component';
@NgModule({
  declarations: [
    AppComponent,
    InstituteListComponent,
    AddInstitueComponent,
    DeleteInstituteComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    NgSelectModule,
    DataTablesModule,
    AuthLibraryModule.forRoot(environment),
    CoreLibraryModule.forRoot(environment),
    BsDropdownModule.forRoot(),
    ToastrModule.forRoot(),
  ],
  providers: [
    AuthGuard,
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
  ],
  entryComponents: [
    AddInstitueComponent,
    DeleteInstituteComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  ModalModule: any;
  DataTablesModule: any
}
