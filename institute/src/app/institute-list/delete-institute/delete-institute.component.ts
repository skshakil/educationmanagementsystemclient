import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { InstituteService } from 'src/app/_coreInstitute/services/institute.service';

@Component({
  selector: 'app-delete-institute',
  templateUrl: './delete-institute.component.html',
  styleUrls: ['./delete-institute.component.css']
})
export class DeleteInstituteComponent implements OnInit {

  static selectedStObj: any;

  onClose: Subject<any> = new Subject();

  constructor(
    public deleteModalRef: BsModalRef,
    private _instituteService: InstituteService,
    private toastr: ToastrService

  ) { }

  ngOnInit(): void {
  
  }

  deleteInstitute() {
    this.onClose.next(true);
    // this._instituteService.deleteInstituteById(DeleteInstituteComponent.selectedStObj.id)
    //   .subscribe((data) => {
    //     this.toastr.success('deleted successfully');
    //     window.location.reload;
    //   })
  }

}
