import { Component, OnInit, ViewChild } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AuthService } from 'auth-library';
import { ToastrService } from 'ngx-toastr';
import { AddInstitueComponent } from './add-institue/add-institue.component';
import { DeleteInstituteComponent } from './delete-institute/delete-institute.component';
import { InstituteService } from '../_coreInstitute/services/institute.service';
import { InstituteEntity } from '../_coreInstitute/model/institute-entity';
@Component({
  selector: 'app-institute-list',
  templateUrl: './institute-list.component.html',
  styleUrls: ['./institute-list.component.css']
})
export class InstituteListComponent implements OnInit {
  showSuccess() {
    this.toastr.success('Toastr Working');
  }

  @ViewChild('institutegridTable', { static: true }) institutegridTable: any;
  instituteModalRef: BsModalRef | undefined;
  deleteModalRef: BsModalRef | undefined;
  instituteList: any;
  institutegridTableObj: any;
  selectedStObj: any;

  constructor(
    private modalService: BsModalService,
    private authService: AuthService,
    private toastr: ToastrService,
    private _instituteService: InstituteService
  ) { }

  ngOnInit(): void {
    this.initInstituteGrid();


  }

  initInstituteGrid() {
    let that = this;
    this.instituteList = $(this.institutegridTable.nativeElement);
    this.institutegridTableObj = this.instituteList.DataTable({
      pagingType: 'full_numbers',
      pageLength: 10,
      serverSide: true,
      processing: true,
      ajax: {
        url:
          environment.baseUrl + environment.authApiUrl + '/institute/gridList',
        type: 'GET',
        data: function (sendData: any) { },
        // beforeSend: function (xhr: any) {
        //   xhr.setRequestHeader(
        //     'Authorization',
        //     'bearer ' + 'd12aec46-f31f-4b73-ab7a-312fd1c7a8f4'
        //   );
        //   xhr.setRequestHeader('Content-Type', 'application/json');
        // },
        beforeSend: function (xhr: any) {
          xhr.setRequestHeader(
            'Authorization',
            'bearer ' + that.authService.getAccessToken()
          );
          xhr.setRequestHeader('Content-Type', 'application/json');
        },
        dataSrc: function (response: any) {
          response.draw = response.obj.draw;
          console.log('Grid: ', response);
          response.recordsTotal = response.obj.recordsTotal;
          response.recordsFiltered = response.obj.recordsFiltered;
          return response.obj.data;
        },
        error: function (request: any) {
          console.log('request.responseText', request.responseText);
        },
      },
      order: [[0, 'desc']],
      columns: [
        {
          title: 'ID',
          data: 'id',
          name: 'id',
        },

        {
          title: 'Institute Id',
          data: 'instituteId',
          name: 'instituteId',
        },
        {
          title: 'Institute Campus',
          data: 'instituteCampus',
          name: 'instituteCampus',
        },
        {
          title: 'Present Address',
          data: 'presentAddress',
          name: 'presentAddress',
        },
        {
          title: 'Contact',
          data: 'contact',
          name: 'contact',
        },
        {
          title: 'Email',
          data: 'email',
          name: 'email',
        },
        {
          className: 'text-center',
          title: 'Action',
          data: '',
          render: (data, type, full) => {
            return '<button class= "ptn btn-info table-btn-all" type="button"><i class="far fa-edit"></i></button> <button class= "ptn btn-info table-btn-all" type="button"><i class="far fa-trash-alt"></i></button> <button class= "ptn btn-info table-btn-all" type="button"><i class="fas fa-cog"></i></button>'
          }
        }
      ],
      responsive: true,
      select: true,
      rowCallback: (row: Node, data: any[] | Object) => {
        $(row).unbind('click');
        $(row).bind('click', () => {
          $(row).removeClass('selected');
          $(row).addClass('selected');
          this.onClickGridData(data);
        });
        return row;
      },
    });
  }

  onClickGridData(data: any): void {
    this._instituteService.findInstitute(data.id).subscribe((institute) => {
      console.log('req: ', institute.obj);
      this.selectedStObj = institute.obj;
    });
    console.log(data)
    this.selectedStObj = data;
  }

  editInstitute() {
    console.log("edit institute called")
    if (this.selectedStObj != null) {
      this.addInstituteModal(this.selectedStObj);
    } else {
      this.toastr.warning("please select an intitute first");
      return;
    }
  }


  addInstituteModal(institute?: any) {
    let initialState = {
      modalTitle: institute ? 'Edit Institute' : 'Add Institute',
      submitTitle: institute ? 'Update' : 'Save',
      receivedInstitute: institute,
    }
    this.instituteModalRef = this.modalService.show(AddInstitueComponent, { initialState, class: 'modal-lg' });
    this.instituteModalRef.content.onClose.subscribe(
      res => {
        if (res) {
          this.institutegridTableObj.draw();
        }
      }
    )
  }


  // deleteInstituteModal() {
  //   this.deleteModalRef = this.modalService.show(DeleteInstituteComponent, { class: 'modal-sm' });
  // }

  deleteInstitute() {
    if (this.selectedStObj != null) {
      this.deleteModalRef = this.modalService.show(DeleteInstituteComponent, { class: 'modal-sm' });
      this.deleteModalRef.content.onClose.subscribe(
        res => {
          if(res) {
            this._instituteService.deleteInstituteById(this.selectedStObj.id).subscribe(
              res=>{
                // this.selectedStObj = res;
                console.log(res);
                this.toastr.success("deleted successfully");
              },err=>{
                this.toastr.error("deleted failed");
              }
            )
          }
        }
      )
    } else {
      this.toastr.error("Please select an institute first");
      return;
    }    
  }
}
