import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { InstituteEntity } from 'src/app/_coreInstitute/model/institute-entity';
import { InstituteService } from 'src/app/_coreInstitute/services/institute.service';
import { assetUrl } from 'src/single-spa/asset-url';

@Component({
  selector: 'app-add-institue',
  templateUrl: './add-institue.component.html',
  styleUrls: ['./add-institue.component.css']
})
export class AddInstitueComponent implements OnInit {
  modalTitle: string = 'Add Institute';
  submitTitle: string = 'Save';
  receivedInstitute: any;

  profileAvatar: any;

  instituteEntity: InstituteEntity = new InstituteEntity();
  response: any;
  // userModalRef: BsModalRef<MessageDialogComponent> | undefined;
  message: string = '';
  onClose: Subject<any> = new Subject();

  constructor(
    public instituteModalRef: BsModalRef,
    private _instituteService: InstituteService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    if (this.receivedInstitute) {
      this.instituteEntity = this.receivedInstitute;

      console.log(this.receivedInstitute);
      
    }
    console.log('ngOnItit called.');
    this.profileAvatar = assetUrl('images/profile-placeholder.jpg');
  }

  submit() {
    if (this.submitTitle == 'Save') {
      this.saveInstitute();
    } else {
      this.updateInstitute();
    }
  }

  updateInstitute() {
    // console.log('update institute is called.');
    let instituteObj = {
      instituteObj: this.instituteEntity,
    };

    console.log('instituteObj: ', instituteObj);
    this._instituteService.updateInstitute(instituteObj).subscribe(
      (data) => {
        // window.location.reload();
        console.log('update institute data: ', data);
        if (data.success) {
          this.instituteModalRef.hide();
          this.onClose.next(true);
        }
      },
      (error) => console.log('error: ' + error)
    );
  }

  saveInstitute() {
    let instituteObj = {
      instituteObj: this.instituteEntity,
    };
    this.response = this._instituteService.saveInstitute(instituteObj).subscribe(
      (data) => {
        console.log(data);
        this.toastr.success("Institute Information Saved Successfully")
      },
      (error) => console.log(error)
    );

    console.log(this.response);
  }


  institute = [
    { id: '1', name: 'Institute 1' },
    { id: '2', name: 'Institute 2' },
    { id: '3', name: 'Institute 3' },
    { id: '4', name: 'Institute 4' },
  ];
  instituteCam = [
    { id: '1', name: 'Institute Campus 1' },
    { id: '2', name: 'Institute Campus 2' },

  ];

}
