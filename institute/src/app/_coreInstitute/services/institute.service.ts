import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InstituteService {
  private SAVE_INSTITUTE_URL = environment.baseUrl + environment.authApiUrl + '/institute/save-info';
  private UPDATE_INSTITUTE_URL = environment.baseUrl + environment.authApiUrl + '/institute/update-info';
  private DELETE_INSTITUTE_URL = environment.baseUrl + environment.authApiUrl + '/institute/delete';
  private FIND_INSTITUTE_URL = environment.baseUrl + environment.authApiUrl + '/institute/find';

  constructor(private httpClient: HttpClient) { }

  saveInstitute(instituteEntity: any): Observable<any> {
    return this.httpClient.post<any>(this.SAVE_INSTITUTE_URL, instituteEntity);
  }

  findInstitute(id: any): Observable<any> {
    return this.httpClient.get(this.FIND_INSTITUTE_URL, {
      params: {
        id: id,
      },
    });
  }

  updateInstitute(instituteEntity: any): Observable<any> {
    return this.httpClient.post<any>(this.UPDATE_INSTITUTE_URL, instituteEntity);
  }

  deleteInstituteById(id: number): Observable<any> {
    return this.httpClient.post<any>(this.DELETE_INSTITUTE_URL, id);
  }
}