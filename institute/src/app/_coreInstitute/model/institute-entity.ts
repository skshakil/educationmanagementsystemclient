export class InstituteEntity {
    id: number | undefined;
    instituteId: string;
    instituteName: string;
    instituteCampus: string;
    presentAddress: string;
    contact: string;
    regNo: string;
    email: string;

}
