import { APP_BASE_HREF, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AuthGuard } from 'auth-library';
import { 
  DashboardLayoutComponent, 
  HomeLayoutComponent,
  GlobalDashboardComponent
} from 'core-library';
import { InstituteListComponent } from './institute-list/institute-list.component';

let appBasePath = environment.basePath+'/'+environment.apiPath;
const routes: Routes = [

  {path:'', redirectTo:'/'+environment.basePath, pathMatch:'full'},
  {
    path:environment.basePath,
    canActivate: [AuthGuard],
    component:DashboardLayoutComponent,
  }, 
  {
    path:environment.basePath+'/dashboard', 
    canActivate:[AuthGuard],
    component:DashboardLayoutComponent,
    children:[
      {
        path:'',
        component:GlobalDashboardComponent
      }
    ]
  },
  {
    path:appBasePath, 
    canActivate: [AuthGuard],
    component:HomeLayoutComponent,
    children:[
      // {
      //   path:'dashboard',
      //   component : DashboardComponent
      // },
    ]
  },
  {
      path:appBasePath, 
      canActivate: [AuthGuard],
      component:HomeLayoutComponent,
      children:[
        {
          path:'institute-list',
          component : InstituteListComponent
        },
      ]
    },
  
    // {
  //   path:appBasePath, 
  //   canActivate: [AuthGuard],
  //   component:HomeLayoutComponent,
  //   children:[
  //     {
  //       path:'dashboard',
  //       component : DashboardComponent
  //     },
  //   ]
  // },
  // {
  //   path:appBasePath, 
  //   canActivate: [AuthGuard],
  //   component:HomeLayoutComponent,
  //   children:[
  //     {
  //       path:'user',
  //       component : UserListComponent
  //     },
  //   ]
  // },
  // {
  //   path: appBasePath+'/child',
  //   component: HomeLayoutComponent,
  //   canActivate : [AuthGuard],
  //   loadChildren: () => 
  //   import('../../../child-module/src/app/child/child.module')
  //   .then(m => m.ChildModule )
  //   .catch(err=> console.log(err)
  //   )
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:false})],
  exports: [RouterModule],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: LocationStrategy, useClass: PathLocationStrategy }
  ]
})
export class AppRoutingModule { }
