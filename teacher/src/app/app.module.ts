import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { DataTablesModule } from 'angular-datatables';
import { ModalModule, BsModalRef } from 'ngx-bootstrap/modal';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { 
  AuthGuard, 
  AuthLibraryModule,
  AuthService,
  TokenInterceptorService
} from 'auth-library';
import {
  CoreLibraryModule
} from 'core-library';
import { TeacherListComponent } from './teacher-list/teacher-list.component';
import { AddTeacherComponent } from './teacher-list/add-teacher/add-teacher.component';
import { DeleteUserComponent } from './teacher-list/delete-user/delete-user.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    TeacherListComponent,
    AddTeacherComponent,
    DeleteUserComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    DataTablesModule,
    ModalModule.forRoot(),
     BsDropdownModule.forRoot(),
    AuthLibraryModule.forRoot(environment),
    CoreLibraryModule.forRoot(environment),
    NgSelectModule,
    ToastrModule.forRoot(),
  ],
    entryComponents: [
    AddTeacherComponent,
    DeleteUserComponent
  ],

  providers: [AuthGuard,AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
   DataTablesModule: any;
   ModalModule: any;
 }
