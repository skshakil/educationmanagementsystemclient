import { Component, OnInit, ViewChild } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AddTeacherComponent } from './add-teacher/add-teacher.component';
import { DeleteUserComponent } from './delete-user/delete-user.component';
import { AuthService } from 'auth-library';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-teacher-list',
  templateUrl: './teacher-list.component.html',
  styleUrls: ['./teacher-list.component.css']
})
export class TeacherListComponent implements OnInit {
  showSuccess() {
    this.toastr.success('Toastr Working');
  }

@ViewChild('teachergridTable', { static: true }) teachergridTable : any;
teacherModalRef: BsModalRef | undefined;
deleteModalRef: BsModalRef | undefined;
  teacherList: any;
 teachergridTableObj: any;
  selectedStObj: any;

  constructor(
    private modalService :BsModalService,
    private authService: AuthService,
     private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.initTeacherGrid();

     
  }

  initTeacherGrid() {
    let that = this;
    this.teacherList = $(this.teachergridTable.nativeElement);
    this.teachergridTableObj = this.teacherList.DataTable({
      pagingType: 'full_numbers',
      pageLength: 10,
      serverSide: true,
      processing: true,
      ajax: {
        url:
          environment.baseUrl + environment.authApiUrl + '/teacher/gridList',
        type: 'GET',
        data: function (sendData: any) {},
        // beforeSend: function (xhr: any) {
        //   xhr.setRequestHeader(
        //     'Authorization',
        //     'bearer ' + 'd12aec46-f31f-4b73-ab7a-312fd1c7a8f4'
        //   );
        //   xhr.setRequestHeader('Content-Type', 'application/json');
        // },
        beforeSend: function (xhr : any) {
          xhr.setRequestHeader(
            'Authorization',
            'bearer ' + that.authService.getAccessToken()
          );
          xhr.setRequestHeader('Content-Type', 'application/json');
        },
        dataSrc: function (response: any) {
          response.draw = response.obj.draw;
          console.log('Grid: ', response);
          response.recordsTotal = response.obj.recordsTotal;
          response.recordsFiltered = response.obj.recordsFiltered;
          return response.obj.data;
        },
        error: function (request: any) {
          console.log('request.responseText', request.responseText);
        },
      },
      order: [[0, 'desc']],
      columns: [
        {
          width: '100px',
          title: 'ID',
          data: 'teacherId',
          name: 'teacherId',
        },
        {
          title: 'Picture',
          data: 'Picture',
          name: 'Picture',
        },
        {
          title: 'Teacher Name',
          data: 'teacherId',
          name: 'TeacherName',
        },
        {
          title: 'Department',
          data: 'Department',
          name: 'Department',
        },
        {
          title: 'Designation',
           data: 'Designation',
          name: 'Designation',
        },
         {
          title: 'Type',
           data: 'Type',
          name: 'Type',
        },
         {
          title: 'Status',
          data: 'Status',
          name: 'Status',
        },
      ],
      responsive: true,
      select: true,
      rowCallback: (row: Node, data: any[] | Object) => {
        $(row).unbind('click');
        $(row).bind('click', () => {
          $(row).removeClass('selected');
          $(row).addClass('selected');
          this.onClickGridData(data);
        });
        return row;
      },
    });
  }

  onClickGridData(data: any): void {
    console.log(data);
    this.selectedStObj = data;
  }

 
  addTeacherModal() {
    this.teacherModalRef = this.modalService.show(AddTeacherComponent, {class: 'modal-lg'});
  }
   deleteTeacherModal() {
    this.deleteModalRef = this.modalService.show(DeleteUserComponent, {class: 'modal-sm'});
  }
}