import { Component, OnInit } from '@angular/core';
import { assetUrl } from 'src/single-spa/asset-url';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-add-teacher',
  templateUrl: './add-teacher.component.html',
  styleUrls: ['./add-teacher.component.css']
})
export class AddTeacherComponent implements OnInit {
 profileAvatar: any;
  constructor(
    public teacherModalRef: BsModalRef,
  ) { }

  ngOnInit(): void {
     this.profileAvatar = assetUrl('images/profile-placeholder.jpg');
  }
 department = [
        { id: 1, name: 'Department 1' },
        { id: 2, name: 'Department 2' },
        { id: 3, name: 'Department 3' },
        { id: 4, name: 'Department 4' },
    ];
Designation = [
        { id: 1, name: 'English'},
        { id: 2, name: 'Bangla' },
       
    ];
    type = [
        { id: 1, name: 'type 1'},
        { id: 2, name: 'type 2' },
       
    ];
    
}
