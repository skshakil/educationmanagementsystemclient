import { APP_BASE_HREF, PathLocationStrategy } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { environment } from 'src/environments/environment';
import { AuthGuard } from 'auth-library';
import { TeacherListComponent } from './teacher-list/teacher-list.component';
import { 
  DashboardLayoutComponent, 
  HomeLayoutComponent,
  GlobalDashboardComponent
} from 'core-library';
import { LocationStrategy } from '@angular/common';

let appBasePath = environment.basePath+'/'+environment.apiPath;
const routes: Routes = [

  {path:'', redirectTo:'/'+environment.basePath, pathMatch:'full'},
  {
    path:environment.basePath,
    canActivate: [AuthGuard],
    component:DashboardLayoutComponent,
  }, 
  {
    path:environment.basePath+'/dashboard', 
    canActivate:[AuthGuard],
    component:DashboardLayoutComponent,
    children:[
      {
        path:'',
        component:GlobalDashboardComponent
      }
    ]
  },
  {
    path:appBasePath, 
    canActivate: [AuthGuard],
    component:HomeLayoutComponent,
    children:[
      {
        path:'dashboard',
        component : DashboardComponent
      },
    ]
  },
{
    path:appBasePath, 
    canActivate: [AuthGuard],
    component:HomeLayoutComponent,
    children:[
      {
        path:'teacher-list',
        component : TeacherListComponent
      },
    ]
  },
//     ** Update from internDev |  line 1
 // 12-08-2021

  // {
  //   path: appBasePath+'/child',
  //   component: HomeLayoutComponent,
  //   canActivate : [AuthGuard],
  //   loadChildren: () => 
  //   import('../../../child-module/src/app/child/child.module')
  //   .then(m => m.ChildModule )
  //   .catch(err=> console.log(err)
  //   )
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:false})],
  exports: [RouterModule],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: LocationStrategy, useClass: PathLocationStrategy }
  ]
})
export class AppRoutingModule { }
